const os = require('os');

const express = require('express');
const logger = require('fluent-logger');

const selfLabel = process.env.LABEL || 'pulse-fluentd';
const selfPort = process.env.PORT || 54545;
const fluentdHost = process.env.FLUENTD_HOST || 'fluentd';
const fluentdPort = process.env.FLUENTD_PORT || 24224;

logger.configure(selfLabel, {
  host: fluentdHost,
  port: fluentdPort,
  timeout: 3.0,
  reconnectInterval: 600000
});

(function pulse() {
  setTimeout(() => {
    const data = {
      timestamp: new Date().toISOString(),
      user: process.env.USER,
      hostname: os.hostname()
    };
    console.info('emitting pulse with data: ', JSON.stringify(data));
    logger.emit(selfLabel, data);
    pulse();
  }, 4321);
})();

const app = express();

app.use('/notify', (req, res) => {
  logger.emit('notify', { message: `${req.method} /notify` });
  res.json('ok');
});

const server = app.listen(selfPort);
server.on('listening', err => {
  logger.emit(selfLabel, {
    message: `listening on http://localhost:${server.address().port}`
  });
});
