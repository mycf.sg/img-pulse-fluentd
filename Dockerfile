FROM node:10.14.2-alpine
WORKDIR /app
COPY ./package.json /app/package.json
COPY ./package-lock.json /app/package-lock.json
RUN npm install
COPY . /app
RUN chown 1000:1000 -R /app
ENTRYPOINT [ "node", "index.js" ]