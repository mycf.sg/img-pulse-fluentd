| :memo: Notice                                   |
|:------------------------------------------------|
| Archived! This has been merged into `ops-base`. |

# Pulse
Simple pulse that emits signals to a FluentD service.

# Usage

## Docker Compose

```yaml
version: "3.5"
services:
  # ...
  pulse:
    image: mycfsg/pulse-fluentd
    environment:
      FLUENTD_HOST: fluentd
      FLUENTD_PORT: 24224
      LABEL: something-unique
      PORT: 54545
    networks:
    - mcf
    user: "${UID:-root}"
    depends_on:
    - fluentd
  fluentd:
    # ...
  # ...
```

# Configuration

| Environment Variable | Description | Default |
| --- | --- | --- |
| FLUENTD_HOST | Hostname of the FluentD service | `"fluentd"` |
| FLUENTD_PORT | Port of the FluentD service | `24224` |
| LABEL | Label for the FluentD logs | `pulse-fluentd` |
| PORT | Port on which to listen on | `54545` |

# Development 

## Version Bumping
To bump a version, [run a pipeline](https://gitlab.com/mycf.sg/img-pulse-fluentd/pipelines/new) with the input variable `VERSION_BUMP_TYPE` set to one of `"major"` or `"minor"`. It defauilts to `"patch"`.

# Re-Use

## Pipeline Configuration

| Pipeline Variable | Description | Example Value |
| --- | --- | -- |
| IMAGE_NAME | Name of the Docker image (eg. for DockerHub, docker.io/org/**__image__**:tag) | `"pulse-fluentd"` |
| IMAGE_NAMESPACE | Namespace of the Docker image (eg. for DockerHub, docker.io/**__org__**/image:tag) | `"mycfsg"` |
| IMAGE_REGISTRY_PASSWORD | Password for the registry user identified in `IMAGE_REGISTRY_USERNAME` | `"password"` |
| IMAGE_REGISTRY_URL | URL of the image registry without the http/https protocol prefix | `"docker.io"` |
| IMAGE_REGISTRY_USERNAME | Username of the registry user | `"user"` |
| SSH_DEPLOY_KEY | Base64 encoded private key corresponding to a registered Deploy Key (see below section on Deploy Key Setup) | `null` |

## Deploy Key Setup
Generate a key using:

```bash
ssh-keygen -t rsa -b 8192 -f ./img-pulse-fluentd -q -N ""
```

Run the private key through a base64 encoding and copy the output:

```bash
cat ./img-pulse-fluentd | base64 -w 0
```

Paste the value in the pipeline variable `SSH_DEPLOY_KEY`.

Copy the contents of the public key and paste the value in the Deploy Keys section of the CI/CD settings page in your repository.

# License
This project is [licensed under the MIT license](./LICENSE).
